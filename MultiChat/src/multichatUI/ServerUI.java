/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multichatUI;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import multichatModel.Server;

/**
 *
 * @author Fernando
 */
public class ServerUI {
    
    static void start() {
        JFrame ServerWindow = new JFrame();
        JPanel top = new JPanel();
        
        JLabel l1 = new JLabel("Nome");
        JTextField name = new JTextField();
        
        JLabel l2 = new JLabel("Endereço");
        JTextField ip = new JTextField();
        
        JPanel clients = new JPanel();
        clients.setLayout(new GridLayout(0,1));
        
        JButton confirm = new JButton("Confirmar");
        confirm.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    System.out.println("A criar servidor.");
                    Server s = new Server(name.getText(), InetAddress.getByName(ip.getText()), clients);
                    System.out.println(s.toString());
                } catch (UnknownHostException ex) {
                    Logger.getLogger(ServerUI.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ServerUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        
        top.setLayout(new GridLayout(2,2));
        
        top.add(l1);
        top.add(name);
        
        top.add(l2);
        top.add(ip);
        
        
        ServerWindow.setLayout(new BorderLayout());
        ServerWindow.add(top, BorderLayout.NORTH);
        ServerWindow.add(clients, BorderLayout.CENTER);
        ServerWindow.add(confirm, BorderLayout.SOUTH);
        
        
        ServerWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ServerWindow.setSize(600, 300);
        ServerWindow.setVisible(true);
        
        
    }
    
}
