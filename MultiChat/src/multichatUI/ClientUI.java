/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multichatUI;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import multichatModel.Client;
import multichatModel.LerFicheiro;

/**
 *
 * @author Fernando
 */
public class ClientUI {

    static void start() {
        ArrayList<InetAddress> ListaDeServidoresPesquisar = new ArrayList();
        JPanel lIP = new JPanel();
        lIP.setLayout(new GridLayout(0,1));
        
        //Adiciona os servidores à lista de pesquisa
        try {
            ArrayList<InetAddress> ListaDeServidoresPesquisar2;
            ListaDeServidoresPesquisar2 = LerFicheiro.readFile();
            for(InetAddress ip : ListaDeServidoresPesquisar2){
                ListaDeServidoresPesquisar.add(ip);
            }
            addListServerToList(lIP, ListaDeServidoresPesquisar);
        } catch (UnknownHostException ex) {
            System.out.println("Hosts desconhecidos");
        } catch (IOException ex) {
            Logger.getLogger(ClientUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        JFrame ClientWindow = new JFrame();
        JPanel top = new JPanel();
        
        JTextArea ListaIP = new JTextArea();
        ListaIP.setEditable(false);
        
        
        JLabel label = new JLabel("Endereço a pesquisar");
        JTextField ip = new JTextField();
        JButton add = new JButton("Adicionar");
        
        JLabel label2 = new JLabel("Nome");
        JTextField nome = new JTextField();
        JButton confirm = new JButton("Confirmar");
        
        
        add.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                //Adiciona um ip à lista de ip's a serem procurados
                try {
                    ListaDeServidoresPesquisar.add(InetAddress.getByName(ip.getText()));
                    System.out.println(ip.getText()+" adicionado à lista de servidores.");
                    addServerToList(lIP, ListaDeServidoresPesquisar, ip.getText());
                    ip.setText("");
                    ClientWindow.revalidate();
                } catch (UnknownHostException ex) {
                    System.out.println("Endereço de Host nao conhecido !");
                }
            }
        });
        
        confirm.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                try {
                    Client c = new Client(nome.getText(), ListaDeServidoresPesquisar);
                } catch (SocketException ex) {
                    Logger.getLogger(ClientUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                ClientWindow.setVisible(false);
            }
        });
        
        top.setLayout(new GridLayout(2,3));
        
        top.add(label2);
        top.add(nome);
        top.add(new JPanel());
        
        top.add(label);
        top.add(ip);
        top.add(add);
        
        ClientWindow.setLayout(new BorderLayout());
        ClientWindow.add(top, BorderLayout.NORTH);
//        ClientWindow.add(ListaIP, BorderLayout.CENTER);
        ClientWindow.add(lIP, BorderLayout.CENTER);
        ClientWindow.add(confirm, BorderLayout.SOUTH);
        
        ClientWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ClientWindow.setSize(600, 200);
        ClientWindow.setVisible(true);
        
    }
    
    //Adiciona UMA LISTA de servidores à lista de servidores a pesquisar
    static void addListServerToList(JPanel lIP, ArrayList<InetAddress> ListaDeServidoresPesquisar){
        for(InetAddress ip : ListaDeServidoresPesquisar){
            JPanel p = new JPanel();
                        p.setLayout(new GridLayout(1,2));
                        JButton del = new JButton("x");
                        del.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent ae) {
                                ListaDeServidoresPesquisar.remove(ip);
                                lIP.remove(p);
                                lIP.revalidate();
                            }
                        });
            p.add(new JLabel(ip.getHostAddress()));
            p.add(del);
            lIP.add(p);
        }
    }
    
    //Adiciona UM servidor à lista de servidores a pesquisar
    static void addServerToList(JPanel lIP, ArrayList<InetAddress> ListaDeServidoresPesquisar, String ip){
            JPanel p = new JPanel();
                        p.setLayout(new GridLayout(1,2));
                        JButton del = new JButton("x");
                        del.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent ae) {
                                try {
                                    ListaDeServidoresPesquisar.remove(InetAddress.getByName(ip));
                                } catch (UnknownHostException ex) {
                                    Logger.getLogger(ClientUI.class.getName()).log(Level.SEVERE, null, ex);
                                }
                                lIP.remove(p);
                                lIP.revalidate();
                            }
                        });
            p.add(new JLabel(ip));
            p.add(del);
            lIP.add(p);
    }
}
