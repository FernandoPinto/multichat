/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multichatUI;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import multichatModel.Client;

/**
 *
 * @author Fernando
 */
public class ChatUI {
    
    Client cliente;
    JFrame chatWindow;
    JTextArea chat;
    JPanel bot;
    JTextField msg;
    JButton send;
    DatagramSocket sockMsg;
    
    public ChatUI(Client c, DatagramSocket sockMsg) throws SocketException{
        this.cliente = c;
        chatWindow = new JFrame();
        chatWindow.setLayout(new BorderLayout());
        this.sockMsg = sockMsg;
        chatWindow.setTitle("MultiChat - "+c.getName());
        
        
        chat = new JTextArea();
        chat.setEditable(false);
        JScrollPane scroll = new JScrollPane(chat,
            JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
            JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        
        
        bot = new JPanel();
        bot.setLayout(new GridLayout(1,2));
        
        msg = new JTextField();
        
                
        send = new JButton("send");
        send.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                String message = msg.getText();
                String command;
                if(message.startsWith("\\")){
                    try{
                        command = message.substring(0, message.indexOf(" "));
                    } catch(StringIndexOutOfBoundsException ex){
                        command = message;
                    }
                    
                    switch(command){
                        case "\\clear":
                            chat.setText("");
                            break;
                        case "\\help":
                            chat.append("\n\n     Lista de comandos existentes\n\n     \\clear - Limpa o chat\n\n     \\server <nome> <mensagem> - Manda uma mensagem para um server específico.\n          ARGUMENTOS\n               nome - Nome ou Endereço do servidor\n              mensagem - Mensagem enviada para o servidor\n\n              Este comando passado sem argumentos irá retornar a lista de servidores disponivéis\n\n"
                                    + "\n\n     \\activate <endereço> - Activa um servidor, passando este a receber mensagens do cliente.\n          ARGUMENTOS\n               endereço - Nome ou Endereço do servidor\n\n"
                                    + "\n\n     \\deactivate <endereço> - Desactiva um servidor, deixando este de receber mensagens do cliente.\n          ARGUMENTOS\n               endereço - Nome ou Endereço do servidor\n\n"
                                    + "\n\n     \\join <endereço> - Adiciona um servidor activo à conversa.\n          ARGUMENTOS\n               endereço - Nome ou Endereço do servidor\n\n");
                            break;
                        case "\\server":
                            if(message.equals("\\server")){
                                //Comando sem parametros, usado para mostrar os servidores
                                if(!c.getIPServidoresActivos().isEmpty()){
                                    chat.append("Servidores Activos\n");
                                    for(InetAddress ip : c.getIPServidoresActivos()){
                                        chat.append("     "+ip.getHostAddress()+"\n");
                                    }
                                }
                                if(!c.getIPServidoresDisponiveis().isEmpty()){
                                    chat.append("Servidores Disponiveis\n");
                                    for(InetAddress ip : c.getIPServidoresDisponiveis()){
                                        chat.append("     "+ip.getHostAddress()+"\n");
                                    }
                                }
                                break;
                            }
                            //Comando com parametros, usado para mandar mensagem a um servidor
                            String serverToSend = message.substring(message.indexOf(" "), message.indexOf(" ", 9));
                            String messageToServer = message.substring(message.indexOf(" ", 9));
                            chat.append("(to "+ serverToSend +") "+ c.getName() +" - " + messageToServer +"\n");
                            String messageToSend = "(from "+ serverToSend +") "+ c.getName() + " - "+ messageToServer;
                            
                            try {
                                byte[] data;
                                data = messageToSend.getBytes();
                                DatagramPacket send = new DatagramPacket(data, messageToSend.length(), InetAddress.getByName(serverToSend), 29799);
                                sockMsg.send(send);
                            } catch (IOException ex) {
                                Logger.getLogger(ChatUI.class.getName()).log(Level.SEVERE, null, ex);
                            }
                            break;
                        case "\\join":
                            if(message.equals("\\join")){
                                chat.append("Falta de argumentos. Digite \\help para mais informação.\n");
                                break;
                            }
                            //Ligar a um servidor
                            InetAddress serverToJoin;
                            try {
                                serverToJoin = InetAddress.getByName(message.substring(message.indexOf(" ")));
                                c.joinServer(serverToJoin);
                            } catch (UnknownHostException ex) {
                                chat.append("\n<--Endereço inválido !-->\n");
                                break;
                            }
                            break;
                        case "\\activate":
                            //Activar um servidor, fazerndo com que este receba mensagens deste cliente
                            InetAddress serverToActivate;
                            try {
                                serverToActivate = InetAddress.getByName(message.substring(message.indexOf(" ")));
                                c.activateServer(serverToActivate);
                            } catch (UnknownHostException ex) {
                                chat.append("\n<--Endereço inválido !-->\n");
                                break;
                            }
                            break;
                        case "\\deactivate":
                            //Desactivar um servidor, fazendo com que este deixe de receber mensagens deste cliente
                            InetAddress serverToDeactivate;
                            try {
                                serverToDeactivate = InetAddress.getByName(message.substring(message.indexOf(" ")));
                                c.deactivateServer(serverToDeactivate);
                            } catch (UnknownHostException ex) {
                                chat.append("\n<--Endereço inválido !-->\n");
                                break;
                            }
                            break;
                        default:
                            chat.append("\n\n     Comando desconhecido !\n     Digite \\help para mais informação sobre os comandos.\n");
                            break;
                    }
                } else {
                    //Caso a mensagem nao seja um comando, esta vai ser mandada para todos os servidores activos
                    try {
                        for(InetAddress ip : c.getIPServidoresActivos()){
                            //Coloca a mensagem no formato Nome - Mensagem
                            String msgFormat = c.getName()+" - "+msg.getText();
                            byte[] data;
                            data = msgFormat.getBytes();
                            DatagramPacket send = new DatagramPacket(data, msgFormat.length(), ip, 29799);
                            sockMsg.send(send);
                        }

                    } catch (SocketException ex) {
                            Logger.getLogger(ClientUI.class.getName()).log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                            Logger.getLogger(ClientUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                }
                msg.setText("");
            }
        });
        chatWindow.getRootPane().setDefaultButton(send);
        
        
        bot.add(msg);
        bot.add(send);
        
        chatWindow.add(scroll, BorderLayout.CENTER);
        chatWindow.add(bot, BorderLayout.SOUTH);
        
        chatWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        chatWindow.setSize(800, 600);
        chatWindow.setVisible(true);
        new Thread(new MsgThread(chat, sockMsg)).start();
    }
    
}

//Thread que aguarda a chegada de mensagens
class MsgThread implements Runnable {
    JTextArea chat;
    DatagramSocket sockMsg;
    
    public MsgThread(JTextArea chat, DatagramSocket sockMsg){
        this.chat = chat;
        this.sockMsg = sockMsg;
    }
    
    public void run() {
        byte[] data = new byte[300];
        String msg;
        
        try{
            while(true){ 
                System.out.println("A espera de mensagem.");
                DatagramPacket pedido = new DatagramPacket(data, data.length);
                sockMsg.receive(pedido);
                System.out.println("Mensagem recebida de "+ pedido.getAddress().getHostAddress() + "- Porto: "+ pedido.getPort());
                String s = new String(pedido.getData(), 0, pedido.getLength());
                //Coloca a mensagem recebida na area do chat
                chat.append(s+"\n");
            }
        } catch (IOException ex) {
            Logger.getLogger(MsgThread.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }  
}

    class MsgTCP implements Runnable
{
private Socket s;
private Client cli;
private PrintWriter pw;
private BufferedReader cliBr;
public MsgTCP(Socket cli_s,Client cli) {
    s=cli_s;
    this.cli=cli;
}
public void run()
{

try {
    cliBr=new BufferedReader(new InputStreamReader(s.getInputStream()));
    String msg=cliBr.readLine();
    DatagramPacket dp=new DatagramPacket(msg.getBytes(), msg.getBytes().length);
    
        cli.getSocket().receive(dp);
    
} catch(IOException ex) { System.out.println("IOException"); }
}
} 
