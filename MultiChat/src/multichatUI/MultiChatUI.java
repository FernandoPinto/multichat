/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multichatUI;

import multichatUI.ClientUI;
import multichatUI.ServerUI;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Fernando
 */
public class MultiChatUI {

    public static void start() {
        JFrame window = new JFrame();
        JLabel label = new JLabel();
        JPanel bottom = new JPanel();
        JButton server = new JButton();
        JButton client = new JButton();
        
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setSize(300, 200);
        window.setLayout(new BorderLayout());
        
        label.setText("Como deseja iniciar a aplicação ?");
        server.setText("Servidor");
        server.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                window.setVisible(false);
                ServerUI.start();
            }
        });
        
        client.setText("Cliente");
        client.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent ae) {
                window.setVisible(false);
                ClientUI.start();
            }
        });
        
        bottom.setLayout(new GridLayout(1,2));
        bottom.add(server);
        bottom.add(client);
        
        window.add(label, BorderLayout.CENTER);
        window.add(bottom, BorderLayout.SOUTH);
        
        window.setVisible(true);
        
        
        
    }
 
    
    
    
    
}
