/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multichatModel;

/**
 *
 * @author 1131298
 */
import java.io.*; 
import java.net.*; 
import java.util.ArrayList;

class Broadcast
{    
private static InetAddress IPdestino;
private DatagramSocket sock=new DatagramSocket();
private static int TIMEOUT=1;
BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

public Broadcast(String ip) throws Exception{
    IPdestino=InetAddress.getByName(ip);
    //tempo antes de time out de ligação
    sock.setSoTimeout(900 * TIMEOUT);
}

public ArrayList<String> listServers() throws Exception{
    ArrayList<String> serverDisplay=new ArrayList<String>();
    sock.setBroadcast(true);
    byte[] data = new byte[300];
    byte[] received=new byte[300];
    DatagramPacket request=new DatagramPacket("".getBytes(), 0, IPdestino, 29601);
    sock.send(request);
    DatagramPacket reply=new DatagramPacket(received,received.length);
    //ciclo para tentativas para ligar a um servidor
    for(int i=0;i<5;i++){
        try{
            sock.receive(reply);
            //armazena num array temporario o que recebeu
            byte[] temp=reply.getData();
            int index=0;
            //verifica a partir de onde é null
            for(int j=0;j<300;j++){
                if(temp[j]==(byte) 0){
                index=j;
                break;
                }
            }
            //nova string apenas com os bytes nao null de array original
            String message;
            message = new String(temp);
            message=message.substring(0, index);
            //alternativa: uset getSocketAddress();
            serverDisplay.add("Nome - " + message + "   IP - " + reply.getAddress());
            
            
            
    } catch (SocketTimeoutException ex){
        System.out.println("Conection timed out");
    }
        
    
    
}
    sock.close();
    return serverDisplay;
}

public InetAddress getIpByHost(String hostName) throws UnknownHostException{
    return InetAddress.getByName(hostName);
    
}
}
