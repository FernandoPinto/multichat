/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multichatModel;
import java.io.*;
import java.net.*; 
import java.util.ArrayList;
/**
 *
 * @author 1131298
 */
public class ServerTCP {
   static InetAddress IPdestino;
static ServerSocket sock;
static ArrayList<Client> lsitaClientes=new ArrayList<Client>();
public static void main(String args[]) throws Exception
{
Socket cliSock;
try { sock = new ServerSocket(29650); }
catch(IOException ex) {
 System.out.println("O porto local esta ocupado.");
 System.exit(1); }
while(true)
 {
 cliSock=sock.accept();
 new Thread(new LigacaoTCP(cliSock)).start();
 }
}
}
class LigacaoTCP implements Runnable
{
private Socket s;
private PrintWriter pw;
private BufferedReader cliBr;
public LigacaoTCP(Socket cli_s) {
    s=cli_s;
}
public void run()
{

try {
    cliBr=new BufferedReader(new InputStreamReader(s.getInputStream()));
    String msg=cliBr.readLine();
    DatagramPacket dp=new DatagramPacket(msg.getBytes(), msg.getBytes().length);
    for(Client c: ServerTCP.lsitaClientes){
        c.getSocket().send(dp);
    }
} catch(IOException ex) { System.out.println("IOException"); }
}
} 

