/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multichatModel;

import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Fernando
 */
public class Server {
    //Numero de porto por onde os clientes se registam
    static final int PORT_CLIENT = 29600;
    
    //Numero de porto por onde os clientes vao pesquisar os servidores
    static final int PORT_SEARCH = 29601;
    
    //Numero de porto por onde as mensagens do chat são enviadas
    static final int PORT_MSG = 29799;
    
    //Listas com informações dos clientes (Funcionam como matriz)
    ArrayList<String> ListaNomeClientes = new ArrayList();
    ArrayList<InetAddress> ListaIPClientes = new ArrayList();
    ArrayList<Integer> ListaPortoClientes = new ArrayList();
    
    //Nome e ip do servidor
    String nome;
    InetAddress ip;
    
    public Server(String nome, InetAddress ip, JPanel clients) throws IOException{
         
        
        this.nome = nome;
        this.ip = ip;
        
        //Inicia thread para ficar à espera de registo de clientes
        new Thread(new ClientThread(ListaNomeClientes, ListaIPClientes, ListaPortoClientes, PORT_CLIENT, clients)).start();
        
        //Inicia thread para ficar à espera de mensagens dos clientes
        new Thread(new MsgThread(ListaNomeClientes, ListaIPClientes, ListaPortoClientes, PORT_MSG)).start();
    }    
    
    //Metodo para adicionar clientes ao servidor, ainda não esta a ser usado
    public void addCliente(String nome, InetAddress ip, int port){
        ListaNomeClientes.add(nome);
        ListaIPClientes.add(ip);
        ListaPortoClientes.add(port);
    }
    
    //Metodo a usar na listagem dos servidores
    public String toString(){
        return nome+" ("+ip.getHostAddress()+")";
    }
    
}

//Thread que ficara à espera da recepcao de mensagens do chat e enviara para os diferentes clientes
class MsgThread implements Runnable {
    ArrayList<String> ListaNomeClientes;
    ArrayList<InetAddress> ListaIPClientes;
    ArrayList<Integer> ListaPortoClientes;
    
    static DatagramSocket sock;
    int porto;
    
    public MsgThread(ArrayList<String> LNC, ArrayList<InetAddress> LIC, ArrayList<Integer> LPC, int p){
        ListaNomeClientes = LNC;
        ListaIPClientes = LIC;
        ListaPortoClientes = LPC;
        porto = p;
    }
    
    public void run() {
        
        try {
            sock = new DatagramSocket(porto);
        } catch (SocketException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try{
            while(true){ 
                byte[] data = new byte[300];
                String msg;
                DatagramPacket pedido = new DatagramPacket(data, data.length);
                sock.receive(pedido);
                System.out.println("Signal recebido (mensagem) de "+ pedido.getAddress().getHostAddress() + " - Porto: "+pedido.getPort());
                String s = new String(data);
                System.out.println(s);
                
                
                System.out.println("A enviar mensagem a todos os clientes.");
                for(int c=0; c<ListaIPClientes.size(); c++){
                    DatagramPacket resposta = new DatagramPacket(data, data.length, ListaIPClientes.get(c), ListaPortoClientes.get(c));
                    sock.send(resposta);
                }
                System.out.println("Mensagens enviadas.");
            }
        } catch (IOException ex) {
            Logger.getLogger(MsgThread.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }  
}

//Thread que fica a espera do registo dos clientes
class ClientThread implements Runnable {
    ArrayList<String> ListaNomeClientes;
    ArrayList<InetAddress> ListaIPClientes;
    ArrayList<Integer> ListaPortoClientes;
    
    JPanel clients;
    
    static DatagramSocket sock;
    int porto;
    
    public ClientThread(ArrayList<String> LNC, ArrayList<InetAddress> LIC, ArrayList<Integer> LPC, int p, JPanel c){
        ListaNomeClientes = LNC;
        ListaIPClientes = LIC;
        ListaPortoClientes = LPC;
        porto = p;
        clients = c;
    }
    
    @Override
    public void run() {
        String nome;
        byte[] data = new byte[300];
        String msg;
        int res, i;
        
        try {
            sock = new DatagramSocket(porto);
        } catch (SocketException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try{
            while(true){ 
                DatagramPacket pedido = new DatagramPacket(data, data.length-1);
                Boolean nameExists = false;
                sock.receive(pedido);
                nome = new String(pedido.getData(), 0, pedido.getLength());
                System.out.println("Signal recebido (registo cliente) de "+ pedido.getAddress().getHostAddress() + " - Porto: "+pedido.getPort());
                //Verifica se o nome ja existe e adiciona o à lista
                if(ListaNomeClientes.isEmpty()){
                    //As tres linhas a seguir vao ser substituiadas pelo metodo addCliente(), a thread vai receber por argumento um 
                    //objecto Server em vez das 3 listas separadas dos nomes, ips, e portos
                    ListaNomeClientes.add(nome);
                    ListaIPClientes.add(pedido.getAddress());
                    ListaPortoClientes.add(pedido.getPort());
                    
                    //So esta a ser adicionado o primeiro cliente à seguinte lista (bug)
                    clients.add(new JLabel(nome));
                    clients.revalidate();
                    
                    String s = new String(data);
                    System.out.println("Cliente registado -> "+s);
                } else {
                    for (String n : ListaNomeClientes) {
                        if(nome.equals(n)){
                            System.out.println("Nome já existe !");
                            nameExists = true;
                        }
                    }
                    if(!nameExists){
                        ListaNomeClientes.add(nome);
                        ListaIPClientes.add(pedido.getAddress());
                        ListaPortoClientes.add(pedido.getPort());
                        String s = new String(data);
                        System.out.println("Cliente registado -> "+s);
                    }
                }
                
            }
        } catch (IOException ex) {
            Logger.getLogger(ClientThread.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }  
}
