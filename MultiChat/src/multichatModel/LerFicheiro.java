/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multichatModel;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 *
 * @author JoãoPedro
 */
public class LerFicheiro {
    
    public static ArrayList<InetAddress> readFile() throws FileNotFoundException, UnknownHostException, IOException{
        
        ArrayList<InetAddress> servidores = new ArrayList();
        BufferedReader br = new BufferedReader(new FileReader("servidores.txt"));
        String line = br.readLine();
        while (line != null) {
            String nome = line.substring(0, line.indexOf(" "));
            InetAddress ip = InetAddress.getByName(line.substring(line.indexOf(" ")+1));
            System.out.println(nome+" - "+ip.getHostAddress());
            servidores.add(ip);
            line = br.readLine();
        }
        return servidores;
    }
    
    
    
    
}
