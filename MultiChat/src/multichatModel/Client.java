/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multichatModel;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import multichatUI.ChatUI;
import multichatUI.ClientUI;

/**
 *
 * @author Fernando
 */
public class Client {
    String name;
    DatagramSocket sockMsg;
    
    //Lista de Servidores (separado em Nomes e IP's)
    ArrayList<String> listaNomeServidoresDisponiveis;
    ArrayList<String> listaNomeServidoresActivos;
    ArrayList<InetAddress> listaIPServidoresDisponiveis;
    ArrayList<InetAddress> listaIPServidoresActivos;
    
    public Client(String name, ArrayList<InetAddress> listaIPServidores) throws SocketException{
        this.name = name;
        this.listaIPServidoresActivos = listaIPServidores;
        this.listaIPServidoresDisponiveis = new ArrayList();
        sockMsg = new DatagramSocket();
        
        //Manda mensagem aos servidores com o seu nome, para proceder ao registo
        try {
            for(InetAddress ip : listaIPServidores){
                    byte[] data;
                    data = name.getBytes();
                    DatagramPacket request = new DatagramPacket(data, name.length(), ip, 29600);
                    System.out.println("A registar cliente no servidor "+ request.getAddress().getHostAddress() + " - Porto: "+request.getPort());
                    sockMsg.send(request);
                    System.out.println("Pedido de registo enviado");
            }
            new ChatUI(this, sockMsg);
                    
        } catch (SocketException ex) {
                Logger.getLogger(ClientUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
                Logger.getLogger(ClientUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getName(){
        return name;
    }
    
    public ArrayList<InetAddress> getIPServidoresActivos(){
        return listaIPServidoresActivos;
    }
    
    public ArrayList<InetAddress> getIPServidoresDisponiveis(){
        return listaIPServidoresDisponiveis;
    }
    
    //Adiciona Servidor à lista de servidores activos
    public void activateServer(InetAddress ip){
        if(listaIPServidoresDisponiveis.contains(ip)){
            listaIPServidoresDisponiveis.remove(ip);
            listaIPServidoresActivos.add(ip);
        }
    }
    
    //Retira Servidor da lista de servidores activos
    public void deactivateServer(InetAddress ip){
        if(listaIPServidoresActivos.contains(ip)){
            listaIPServidoresActivos.remove(ip);
            listaIPServidoresDisponiveis.add(ip);
        }
    }
    
    
    public DatagramSocket getSocket(){
        return this.sockMsg;
    }

    //Manda uma mensagem a um servidor com o nome do cliente, para se registar
    public void joinServer(InetAddress ip){
        
        try {
                    byte[] data;
                    data = name.getBytes();
                    DatagramPacket request = new DatagramPacket(data, name.length(), ip, 29600);
                    System.out.println("A registar cliente no servidor "+ request.getAddress().getHostAddress() + " - Porto: "+request.getPort());
                    sockMsg.send(request);
                    System.out.println("Pedido de registo enviado");
                    listaIPServidoresActivos.add(ip);
                    
        } catch (SocketException ex) {
                Logger.getLogger(ClientUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
                Logger.getLogger(ClientUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    

    
   
    
}
